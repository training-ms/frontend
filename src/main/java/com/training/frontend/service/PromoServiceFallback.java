package com.training.frontend.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.training.frontend.dto.Product;

@Component
public class PromoServiceFallback implements PromoService{

	@Override
	public Iterable<Product> dataSemuaProduk() {
		// TODO Auto-generated method stub
		return new ArrayList<>();
	}

	@Override
	public Map<String, Object> backendInfo() {
		// TODO Auto-generated method stub
		Map<String, Object> hasilFallback = new HashMap<>();
        hasilFallback.put("host", "localhost");
        hasilFallback.put("ip", "127.0.0.1");
        hasilFallback.put("port", -1);
        return hasilFallback;

	}

}
