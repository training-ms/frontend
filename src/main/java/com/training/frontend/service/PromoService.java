package com.training.frontend.service;

import java.util.Map;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

import com.training.frontend.dto.Product;

@FeignClient(name="promo", fallback = PromoServiceFallback.class)
public interface PromoService {
	@GetMapping("/product/")
    Iterable<Product> dataSemuaProduk();
	
	@GetMapping("/hostinfo")
    public Map<String, Object> backendInfo();


}
