package com.training.frontend.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

import com.training.frontend.service.PromoService;

@Controller
public class ProductController {

	@Autowired private PromoService promoService;
	
    @GetMapping("/product/list")
    public ModelMap daftarProduk() {
        return new ModelMap().addAttribute("dataProduk",
                promoService.dataSemuaProduk());

    }
    
    @GetMapping("/backend")
    public ModelMap backendInfo() {
        return new ModelMap()
                .addAttribute("hostInfo",
                        promoService.backendInfo());
    }


}
